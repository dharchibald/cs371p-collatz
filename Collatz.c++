// --------------------------------
// projects/c++/collatz/Collatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// --------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

#define MAX_INPUT 1000000

using namespace std;

int collatz_recursion (long);

int cycles[MAX_INPUT] = {};

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {

    assert (0 < i && i < MAX_INPUT);
    assert (0 < j && j < MAX_INPUT);

    int max = 0;

    int low;
    int high;
    int lowerBound;

    if (i < j)
    {
        low = i;
        high = j;
    }
    else
    {
        low = j;
        high = i;
    }

    lowerBound = (high / 2) + 1;
    if (low < lowerBound)
        low = lowerBound;

    for (long index = low; index <= high; ++index)
    {
        int result = collatz_recursion (index) + 1;
        cycles[index] = result;
        if (result > max)
            max = result;
    }

    return max;
}

int collatz_recursion (long num) {

    int result = 0;
    bool skip_flag = false;

    /* Base case reached */
    if (num == 1)
        return 0;

    else
    {
        /* Cache already contains answer */
        if (num < MAX_INPUT && cycles[num] != 0)
            return cycles[num] - 1;

        if (num % 2 == 0)
            num /= 2;
        else
        {
            num = ((num*3) + 1)/2;
            skip_flag = true;
        }

        result = collatz_recursion (num) + 1;
        if (num < MAX_INPUT)
            cycles[num] = result;

        if (skip_flag)
            ++result;

        return result;
    }
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
