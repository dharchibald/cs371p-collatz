// ------------------------------------
// projects/c++/collatz/TestCollatz.c++
// Copyright (C) 2018
// Glenn P. Downing
// ------------------------------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Collatz.h"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}

// ----
// Custom-made unit tests
// ----

// Test when i > j
TEST(CollatzFixture, eval_5) {
    const int v = collatz_eval(5, 1);
    ASSERT_EQ(v, 8);
}

// Test maximum range of i/j
TEST(CollatzFixture, eval_6) {
    const int v = collatz_eval(999999, 999999);
    ASSERT_EQ(v, 259);
}

TEST(CollatzFixture, eval_7) {
    const int v = collatz_eval(1, 999);
    ASSERT_EQ(v, 179);
}

TEST(CollatzFixture, eval_8) {
    const int v = collatz_eval(262, 351);
    ASSERT_EQ(v, 144);
}

TEST(CollatzFixture, eval_9) {
    const int v = collatz_eval(8303, 8508);
    ASSERT_EQ(v, 234);
}

TEST(CollatzFixture, eval_10) {
    const int v = collatz_eval(64751, 65000);
    ASSERT_EQ(v, 268);
}

TEST(CollatzFixture, eval_11) {
    const int v = collatz_eval(89127, 90000);
    ASSERT_EQ(v, 302);
}

TEST(CollatzFixture, eval_12) {
    const int v = collatz_eval(98219, 99000);
    ASSERT_EQ(v, 284);
}

TEST(CollatzFixture, eval_13) {
    const int v = collatz_eval(58310, 58500);
    ASSERT_EQ(v, 242);
}

TEST(CollatzFixture, eval_14) {
    const int v = collatz_eval (66808, 67000);
    ASSERT_EQ(v, 237);
}

TEST(CollatzFixture, eval_15) {
    const int v = collatz_eval (43258, 40000);
    ASSERT_EQ(v, 288);
}

TEST(CollatzFixture, eval_16) {
    const int v = collatz_eval (50000, 60000);
    ASSERT_EQ(v, 340);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}
